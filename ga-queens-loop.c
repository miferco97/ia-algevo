#include "ga.h"
#include <grp.h>
#include <pwd.h>
#include <sys/types.h>
#include <sys/wait.h>
#include <unistd.h>
#include <stdlib.h>
#include <stdio.h>
#include <string.h>
#include <time.h>



// if PLOT_TERMINAL == 0 -> NO se plotea en el terminal
// if PLOT_TERMINAL == 1 -> SI se plotea en el terminal
#define PLOT_TERMINAL 0
/*--- Forward declarations ---*/
void chrom2chessboard(Chrom_Ptr c, char **tablero);
int cuentaamenazas(char **tablero);
void plotData (FILE *f ,Chrom_Ptr c, char **tablero);
int obj_fun();

#define MU_VEC_LENGTH 3
#define X_VEC_LENGTH 3
#define POOL_VEC_LENGTH 3
#define QUEEN_VEC_LENGTH 5

// Variables globales

// Parametros algoritmos
float mu_rate_vec[MU_VEC_LENGTH] = {0.7, 0.8, 0.9};
float x_rate_vec[X_VEC_LENGTH] = {0.6, 0.7, 0.8};
int pool_size_vec[POOL_VEC_LENGTH] = {100, 150, 200};
// int queen_num_vec[QUEEN_VEC_LENGTH] = {4, 5, 6, 7, 8};
int queen_num_vec[QUEEN_VEC_LENGTH] = {5, 10, 15, 20, 25};

// int max_iter[QUEEN_VEC_LENGTH]= {100,150,200,250,300};
int max_iter[QUEEN_VEC_LENGTH]= {100,150,200,250,300};

// Datos algoritmo
int tipo;
int maxr;
int NumIter;

char **tablero;

// Configuracion por defecto del GA
GA_Info_Ptr ga_info;

/*----------------------------------------------------------------------------
| main()
----------------------------------------------------------------------------*/
void main()
{
    // tiempo en el que comienza la ejecución

    pid_t pid,pid_proceso_lento,pid_flag;
    int index;
    int status;
    time_t t;



    int nProcess= QUEEN_VEC_LENGTH; // Variable para contar el n de hijos que se crean

    while (nProcess > 0){
        switch(pid = fork())
        {
            case (pid_t) -1:
            perror("fork");
            exit(-1);
            break;
            case (pid_t) 0:
            // Proceso HIJO
            // Recabar el indice del proceso para acceder al nReinas correspondiente
            index = nProcess-1;

            // Flag para evitar que se generen hijos de hijos
            nProcess=0;
            break;

            default:
            //Proceso PADRE
            if (nProcess==QUEEN_VEC_LENGTH){
                pid_proceso_lento= pid;
            }
            nProcess--;
            index=-1;

            break;
        }
    }

    if (index == -1){
        clock_t start = clock();
        nProcess--; // Disminuye el contador
        index = nProcess;

        do {
               if ((pid_flag = waitpid(pid_proceso_lento, &status, WNOHANG)) == -1)
                 perror("wait() error");
               else if (pid_flag == 0) {
                    time(&t);
                    //printf("child is still running at %s", ctime(&t));
                    sleep(3);
                    // printf("Tiempo transcurrido: %f \n",((double)clock() - start) / CLOCKS_PER_SEC);

                }
               else {
                 if (WIFEXITED(status)){
                   sleep(1);
                   printf("Ejecutado con éxito\n");
               }
                 else puts("child did not exit successfully");
               }
             } while (pid_flag == 0);

             exit(0);
         }

    printf("Process %d created \n", index);

    // Codigo a ejecutar en cada Proceso
    // Cada proceso sabe cual és a traves de la variable index

    maxr = queen_num_vec[index]; // Dimensiones tablero N x N

    //Fichero en el que guardar los resultados
    FILE *f;
    char nombre[20];
    sprintf(nombre,"results_%d.txt",maxr);
    f = fopen(nombre,"w");
    fprintf(f,"PROCESO %d\n\n",index);

    // encoding == 0 -> DT_BIT
    // encoding == 1 -> DT_INT_PERM

    // Reserva de memoria dinámica para el tablero
    tablero = (char **)malloc(maxr * sizeof(char *));
    for (int i=0; i<maxr; i++){
             tablero[i] = (char *)malloc(maxr * sizeof(char));
    }

    // Iteraciones en el encoding
    for(int encoding=0; encoding<2;encoding++){

        // Iteraciones sobre la probabilidad de mutacion
        for(int mutationProbIndex=0; mutationProbIndex<MU_VEC_LENGTH;mutationProbIndex++){

            // Iteraciones sobre la probabilidad de cruce
            for(int crossoverProbIndex=0;crossoverProbIndex<X_VEC_LENGTH;crossoverProbIndex++){

                // Iteraciones sobre el tamaño de la Pool
                for(int poolLengthIndex=0; poolLengthIndex<POOL_VEC_LENGTH; poolLengthIndex++){
		    //Numero de iteraciones hasta convergencia
		    NumIter=0;

                    //Valores por defecto
                    ga_info = GA_config("GAconfig", obj_fun);

                    // modificacion en la Configuracion del GA para cada iteracion

                    // configure nSteps
                    ga_info->use_convergence = FALSE;
                    ga_info->max_iter = max_iter[index];

                    // Eleccion del encoding

                    if(encoding == 0){
                        // modificacion de la Configuracion del GA
                        ga_info->datatype=(int)DT_BIT;
                        ga_info->chrom_len=maxr*maxr;
                        // Crossover for N-BIT config
                        X_select(ga_info, "simple");
                    }

                    else{
                        // modificacion de la Configuracion del GA
                        ga_info->datatype=(int)DT_INT_PERM;
                        ga_info->chrom_len=maxr;
                        // Crossover for INT_PERM config
                        X_select(ga_info, "order1");
                    }

                    // type check
                    tipo=ga_info->datatype;
                    if(!(ga_info->datatype == DT_BIT || ga_info->datatype == DT_INT_PERM)){
                        printf("Something went wrong...%d\n",ga_info->datatype);
                        exit(-1);
                    }

                    // modificacion del resto de parámetros
                    ga_info->mu_rate=mu_rate_vec[mutationProbIndex];
                    ga_info->x_rate=x_rate_vec[crossoverProbIndex];
                    ga_info->pool_size=pool_size_vec[poolLengthIndex];

                    // printf("%d, chrom_len: %d\n",ga_info->datatype,ga_info->chrom_len);
                    // printf("Crossover:%d\n",ga_info->X_fun);
                    
                    //Inicializacion de la semilla aleatoria
		    ga_info->rand_seed = time(NULL)%10*(poolLengthIndex+1)*(crossoverProbIndex+1)*(mutationProbIndex+1);

                    // Write parameters configuration
                    if (encoding == 0){
                        // Archivo de resultados
                        fprintf(f,"DATA TYPE => BIT\n\n");
                    }
                    else{
                         fprintf(f,"DATA TYPE => PERM\n\n");
                    }

                    fprintf(f,"\tMU_RATE = %f\n",ga_info->mu_rate);
                    fprintf(f,"\tX_RATE = %f\n\n",ga_info->x_rate);
                    fprintf(f,"\tPOOL_SIZE = %d\n\n",ga_info->pool_size);

                    /*--- Run the GA ---*/
                    GA_run(ga_info);

#if PLOT_TERMINAL == 1
                    printf("\nBest chrom:  ");
#endif
                    fprintf(f,"\t\tBest chrom:  ");

                    // Muestra el mejor genoma

                    for(int i=0;i<ga_info->chrom_len;i++){

#if PLOT_TERMINAL == 1
                        printf("%d  ",(int) ga_info->best->gene[i]);
#endif
                        fprintf(f, "%d  ", (int) ga_info->best->gene[i]);
                    }

                    // Guarda el fitness que ha alcanzado el mejor genoma
#if PLOT_TERMINAL == 1
                    printf("   (fitness: %g)\n\n",ga_info->best->fitness);
#endif
                    fprintf(f, "\t\t   (fitness: %g)",ga_info->best->fitness);
                    fprintf(f,"\tNumIter = %d\n\n", NumIter-1);

                    plotData(f,ga_info->best,tablero);
		    RP_final(ga_info);

                }
            }
        }
        fprintf(f,"\n\n\n");


    }

        fclose(f);
        printf("Proceso %d  finalizado\n",index);
        exit(0);
}


/**---- END OF MAIN  ----**/


/*----------------------------------------------------------------------------
| obj_fun() - user specified objective function
----------------------------------------------------------------------------*/
int obj_fun(Chrom_Ptr chrom)
{
    int i;
    double val = 0.0;


    chrom2chessboard(chrom,(char **)tablero);

    int nAmenazas=cuentaamenazas((char **)tablero);
    // printf("nAmenazas = %d\n",nAmenazas );
    int dist= 0;
    int nReinas=0;

    if (tipo==DT_BIT){
        for(int j=0;j<maxr;j++){
            //printf("J=%d\n",j );
            for(int k=0;k<maxr;k++){
                //printf("K=%d\n",k);
                nReinas+=chrom->gene[j*(maxr)+k];
            }
        }
        dist= maxr - nReinas;
        dist=(dist>0)?dist:-dist;
    }

    chrom->fitness = nAmenazas + 10*dist;
    
    //Guardar la iteración en la que converge
    if(chrom->fitness==0&&NumIter==0){
	NumIter=ga_info->iter+2;
    }

    return 0;

}

/*----------------------------------------------------------------------------
| FUNCIONES
----------------------------------------------------------------------------*/


void chrom2chessboard(Chrom_Ptr c, char **tablero)
{
    int i,j;
    int size=maxr;
    if(tipo==DT_BIT){
        for(i=0;i<size;i++){
            for(j=0;j<size;j++){
                tablero[i][j] = c->gene[i*size+j];
            }
        }
    }
    else if(tipo==DT_INT_PERM)
    {
        for(i=0;i<size;i++){
            for(j=0;j<size;j++)tablero[i][j] = 0;
        }
        for(i=0;i<size;i++){
            tablero[i][(int)c->gene[i]-1] = 1;
        }
    }
    else printf("WTF???\n");
}

int cuentaamenazas(char **tablero)
{
    int i,sum,f,c;
    int amenazas = 0;
    int n = maxr;

    // checking rows
    for(f=0;f<n;f++)
    {
        sum = 0;
        for(c=0;c<n;c++)
        sum += (int)tablero[f][c];
        if(sum>1) amenazas += sum*(sum-1)/2;
    }

    // checking columns
    for(c=0;c<n;c++)
    {
        sum = 0;
        for(f=0;f<n;f++)
        sum += (int)tablero[f][c];
        if(sum>1) amenazas += sum*(sum-1)/2;
    }

    // checking diagonals
    for(f=0;f<n;f++)
    for(c=0;c<n;c++)
    {
        sum = 0;
        for(i=0;i<n;i++)
        {
            if(f+i<n && c+i<n)
            sum += (int)tablero[f+i][c+i];
        }
        if(sum>1) amenazas += sum*(sum-1)/2;

        sum = 0;
        for(i=0;i<n;i++)
        {
            if(f+i<n && c-i>=0)
            sum += (int)tablero[f+i][c-i];
        }
        if(sum>1) amenazas += sum*(sum-1)/2;

        sum = 0;
        for(i=0;i<n;i++)
        { if(f-i>=0 && c+i<n)
            sum += (int)tablero[f-i][c+i];
        }
        if(sum>1) amenazas += sum*(sum-1)/2;

        sum = 0;
        for(i=0;i<n;i++)
        {if(f-i>=0 && c-i>=0)
            sum += (int)tablero[f-i][c-i];
        }
        if(sum>1) amenazas += sum*(sum-1)/2;

    }

    return amenazas;
}


void plotData (FILE *f ,Chrom_Ptr c, char **tablero){
    int size=maxr;
    chrom2chessboard(c,tablero);
    for (int j=0;j<size;j++){
        fprintf(f,"\t\t");
        for (int k=0;k<size;k++){

#if PLOT_TERMINAL == 1
            printf(" %d ", (int )tablero[j][k]);
#endif

            fprintf(f, " %d ", (int )tablero[j][k]);
        }

#if PLOT_TERMINAL == 1
        printf("\n");
#endif

        fprintf(f, "\n");
    }
    fprintf(f,"\n");

}
