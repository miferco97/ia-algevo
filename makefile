



#
# How to make .c into .o
#
.c.o: ga.h
	cc  -c $<


ga-queens: ga-queens.o
	gcc ga-queens-loop.c -o ./ga-queens-loop  -L./libga  -lGA -lm

clean:
	rm -f *~
	rm -f *.o
